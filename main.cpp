/*
Galerkin Method implementation (FEA Mockup)
Author: Joel King, UD Mathematics
Oct. - Nov. 2020

Boost verison 1.74
Eigen version 3.3.8

The Galerkin method uses a finite-dimensional subset of hilbert spaces to approximate the solution to a given PDE problem. Basic Linear Algebra Subroutines (BLAS) methods will be used
with an implementation provided by a common per-reviewed open-source code-base, Eigen (eigen.tuxfamily.org). Other math utilities provided by Boost libraries (www.Boost.org). Both of these libraries can be used as header-only libraries. 


We will implement the Galerkin method with piece-polynomial functions (aka Finite Element Method) and find a solution for the heat equation in two dimensions on a square piece of material with Dirichlet boundary conditions.

The Heat equation is: (For a function u : I X R -> R) Du/Dt = laplacian * u = del * del * u. 

We consider the concrete case where the boundaries are defined by initial conditions: f(x,t) = f(x_0, t) = f(x_1, t) = 5. 

*/


#include <iostream>
#include <vector>
#include <functional>

#include "Boost/math/tr1.hpp"
#include "boost/math/differentiation/finite_difference.hpp"
#include "Boost/math/quadrature/gauss.hpp"

#include "Eigen/Sparse"
#include "Eigen/Cholesky"

#include "Utilities.hpp"
#include "RungeKuttaMethod.hpp"
#include "PostProcessing.hpp"




/*
Takes a callable expresison and returns another callable expression of a single variable, approx. equal to the derivative.
This could be done where needed, however it is technical enough that some amount of abstraction is helpful.
*/


double grad(std::function<double(double)> pfunc, double px)
{
	return boost::math::differentiation::finite_difference_derivative(pfunc, px);
}



/*
Bilinear form
*/

double bilinearForm(double i, double j, Legendre_Polynomial lgpoly) // Bilinear Form, integrated over reference interval.
{
	std::function<double(double)> i_bnd = std::bind(lgpoly, std::placeholders::_1, i); // Binds parameters so it is easier to use.
	std::function<double(double)> j_bnd = std::bind(lgpoly, std::placeholders::_1, j);
	
	
	//Actually writing the bi-linear form for our problem. 
	//We must use a lambda function to pass into our integration method.
	auto form2 = [i_bnd, j_bnd](double x) { return grad(i_bnd, x)* grad(j_bnd,x); };
	
	// Integrate over the reference interval (-1,1)
	double result = boost::math::quadrature::gauss<double, 50>::integrate(form2);
	
	return result;
}

double ForceEntry(double i, double j, Legendre_Polynomial lgpoly)
{
	// In this case, our F vector has the same entry "form" as our bilinear form.
	// We use this name only to clarify what is being done when it is invoked.
	return bilinearForm(i,j,lgpoly);
}

double MassEntries(double i, double j, Legendre_Polynomial lgpoly)
{
	std::function<double(double)> i_bnd = std::bind(lgpoly, std::placeholders::_1, i); // Binds parameters so it is easier to use.
	std::function<double(double)> j_bnd = std::bind(lgpoly, std::placeholders::_1, j);
	
	auto MForm =  [i_bnd, j_bnd](double x) { return i_bnd(x)*j_bnd(x); };
	
	double result = boost::math::quadrature::gauss<double, 50>::integrate(MForm);
	return result;
	
}





int main()
{
	const double LEFT_BOUND = -5;
	const double RIGHT_BOUND = 5; // Limits of the bar, total length 10 units.
	
	const double HOT_DIR_BC = 3.0;
	const double COLD_DIR_BC = 2.0;
	const double AMBIANT_BC = 4.0;
	
	
	const int NUM_OF_ELEMENTS= 5; // Number of elements to use, equal lengths, distributed equally. 
	const int POLY_DEGREE = 7; // The Degree of shape functions to use on each element.
	
	std::vector<Element> ElementArray(NUM_OF_ELEMENTS);
	
	Legendre_Polynomial lg_Basis = GenerateBasis(POLY_DEGREE);
	
	/*
	Each element has p+1 shape functions. So the size of our matrix system
	will be square with (p+1)*NUM_OF_ELEMENTS rows and columns.
	*/
	
	Eigen::MatrixXd Stiffness( (POLY_DEGREE+1)*NUM_OF_ELEMENTS, (POLY_DEGREE+1)*NUM_OF_ELEMENTS);
	Eigen::MatrixXd MassM(Stiffness.rows(), Stiffness.cols());
	// Stiffness and Mass matrix should be zero by default:
	
	// Force matrix, also zero by default.
	Eigen::VectorXd ForceV( Stiffness.rows() );
	
	
	for(int i=0;  i < Stiffness.rows(); i++ )
	{
		for(int j=0; j < Stiffness.rows(); j++)
		{
			Stiffness(i,j) = 0.0;
			MassM(i,j) = 0.0;
		}
		ForceV(i) = 0.0;
	}
	
	/*
	Temp_Nought(0) = HOT_DIR_BC;
	Temp_Nought( Temp_Nought.rows()-1 ) = COLD_DIR_BC;
	for(int i=1; i < Temp_Nought.rows()-1; i++)
	{
		Temp_Nought(i) = AMBIANT_BC;
	}
	*/
	
	int count = 1;
	
	
	ElementArray[0].GLOBAL_VERTEX_ID[0] = count; count++;
	ElementArray[0].GLOBAL_VERTEX_ID[1] = count;
	

	for(int i = 1; i < NUM_OF_ELEMENTS; i++)
	{
		// First set unique global ids for each shape function.
		// Look at vertex first:
		ElementArray[i].GLOBAL_VERTEX_ID[0] = count; count++;
		ElementArray[i].GLOBAL_VERTEX_ID[1] = count;
	}
	count++;
	for(int i = 0; i < NUM_OF_ELEMENTS; i++)
	{
		// First set unique global ids for each shape function.
		// Now enumerate interior shape functions.
		int ShapeCounts = POLY_DEGREE - 1; // Two functions for each vertex
		for(int k = 0; k < ShapeCounts; k++)
		{
			ElementArray[i].BubbleID.push_back(count);
			std::cout << count << std::endl;
			count++;
		}
		
	}
	
	// Compute the "Jacobian" for each element, used to transform 
	//integration results *from* the reference domain.
	
	//Loop over elements.
	for(int i=0; i < NUM_OF_ELEMENTS; i++)
	{
		double ElementWidth = (RIGHT_BOUND - LEFT_BOUND)/NUM_OF_ELEMENTS;
		ElementArray[i].LeftPoint = LEFT_BOUND + ElementWidth*i;
		ElementArray[i].RightPoint = ElementArray[i].LeftPoint + ElementWidth;
		ElementArray[i].Jacobian(); // Simply takes the derivative of the reference map.
		
	}
	
	//With each shape function given a unique, global identifier, we specificy boundary
	// conditions.
	
	ElementArray[0].DIR_FLAG[0] = true; // Left most vertex
	ElementArray[ ElementArray.size()-1 ].DIR_FLAG[1] = true; // Rightmost vertex.
	
	/**************
	Recall we have set, HOT_DIR_BC and COLD_DIR_BC
	and AMBIANT_BC
	
	***************/
	
	// Initial COndition, temperature of the rod at t=0 on each node.
	auto IC_Value =  [COLD_DIR_BC, HOT_DIR_BC, AMBIANT_BC](double x) { if(x==-5.0) {return HOT_DIR_BC; } if(x==5){ return COLD_DIR_BC; } else { return AMBIANT_BC;} };
	Eigen::VectorXd Temp_Nought = InterpolateFunction(IC_Value, lg_Basis, ElementArray);
	
	
	
	
	//Now assemble the linear system.
	//Element loop
	for(int elem = 0; elem < NUM_OF_ELEMENTS; elem++)
	{
		// Vertex loop. (Test Function)
		for(int iv = 0; iv <= 1; iv++ ) // Each element has two vertices.
		{
			int m1 = ElementArray[elem].GLOBAL_VERTEX_ID[iv];
			int Local_LG_Func = (iv==0) ? 1 : POLY_DEGREE+1;
			
			
			
			
			for(int j=0; j<=1; j++) // Vertex basis function
			{
				int test_Func = (j==0) ? 1 : POLY_DEGREE+1;
			
				int m2 = ElementArray[elem].GLOBAL_VERTEX_ID[j];
				Stiffness(m1,m2) += bilinearForm( Local_LG_Func , test_Func, lg_Basis ) / ElementArray[elem].Jac; // Our bilinear form is symmetric, so we exploit that.
				
				//Mass Matrix, multiply by Jacobian: 
				MassM(m1, m2) += MassEntries( Local_LG_Func , test_Func, lg_Basis ) * ElementArray[elem].Jac;
				
			}
			
			for(int k=1; k < POLY_DEGREE-1; k++ ) // Bubble basis function
			{
				int m2 = ElementArray[elem].BubbleID[k-1];
				
				//Populate Stiffness:
				Stiffness(m1,m2) += bilinearForm(Local_LG_Func, k+1, lg_Basis) / ElementArray[elem].Jac;  

				//Populate Mass:
				MassM(m1, m2) += MassEntries( Local_LG_Func , k+1, lg_Basis ) * ElementArray[elem].Jac;
			}
			
			if( ElementArray[elem].DIR_FLAG[0] )
			{
				ForceV(m1) += -1.0 * HOT_DIR_BC * (1/(ElementArray[elem].Jac*ElementArray[elem].Jac)) * ForceEntry(1 , Local_LG_Func, lg_Basis);
			}
			if( ElementArray[elem].DIR_FLAG[1] )
			{
				ForceV(m1) += -1.0 * COLD_DIR_BC * (1/(ElementArray[elem].Jac*ElementArray[elem].Jac)) * ForceEntry( POLY_DEGREE+1 , Local_LG_Func, lg_Basis);
			}
			
			
		} // End vertex test function loop
		
		//Bubble loop. (Test Function)
		for(int ib=1; ib < POLY_DEGREE-1; ib++)
		{
			int m1 = ElementArray[elem].BubbleID[ib-1];
			
			
			for(int ibv=0; ibv <=1; ibv++) //vertex basis loop
			{
				int m2 = ElementArray[elem].GLOBAL_VERTEX_ID[ibv];
				int Local_LG_Func = (ibv==0) ? 1 : POLY_DEGREE+1;
				
				//Stiffness:
				Stiffness(m1,m2) += bilinearForm( ib+1, Local_LG_Func, lg_Basis ) / ElementArray[elem].Jac;
				
				//Mass Matrix:
				MassM(m1, m2) += MassEntries( Local_LG_Func , ib+1, lg_Basis ) * ElementArray[elem].Jac;
			}
			
			for(int ibb=0; ibb < POLY_DEGREE-1; ibb++) // Bubble basis functions
			{
				int m2 = ElementArray[elem].BubbleID[ibb];
				
				//Stiffness:
				Stiffness(m1,m2) += bilinearForm( ib+1, ibb+1, lg_Basis ) / ElementArray[elem].Jac;
				
				//Mass Matrix: 
				MassM(m1, m2) += MassEntries( ib+1 , ibb+1, lg_Basis ) * ElementArray[elem].Jac;
			}
			
			if( ElementArray[elem].DIR_FLAG[0] )
			{
				ForceV(m1) += -1.0 * HOT_DIR_BC * (1/(ElementArray[elem].Jac*ElementArray[elem].Jac)) * ForceEntry(1 , ib+1, lg_Basis);
			}
			if( ElementArray[elem].DIR_FLAG[1] )
			{
				ForceV(m1) += -1.0 * COLD_DIR_BC * (1/(ElementArray[elem].Jac*ElementArray[elem].Jac)) * ForceEntry( POLY_DEGREE+1 , ib+1, lg_Basis);
			}
			
			
		}// End bubble test function loop
		
		
	} // End Element Loop
	
	Eigen::MatrixXd Temp_Prime = Eigen::LDLT<Eigen::MatrixXd>(MassM).solve( ForceV + Stiffness*Temp_Nought);
	
	for(int i =0 ; i < Temp_Prime.rows(); i++)
	{
		std::cout << "[" << Temp_Prime(i) << "]";
		if(i%6 ==0)
			std::cout << std::endl;
	}
	

	return 0;
}