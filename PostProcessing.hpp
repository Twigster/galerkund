#include <iostream>
#include <functional>
#include <algorithm>

#include "Eigen/Dense"

#include "Utilities.hpp"


double ValueAtPoint(Eigen::VectorXd Coefs, std::vector<Element> Mesh, Legendre_Polynomial basis, double point, int POLY_DEGREE)
{
	double result = 0;
	
	//We first identify which element this point is in:
	for(int i=0; i < Mesh.size(); i++)
	{
		if(Mesh[i].LeftPoint <= point && Mesh[i].RightPoint >= point)
		{
			for(int i=0; i < 1; i)
			{
				int lc = (i==0) ? 1 : POLY_DEGREE+1;
				result += Coefs( Mesh[i].GLOBAL_VERTEX_ID[i] )* basis(point,lc);
			}
			for(int k =0; k < POLY_DEGREE-1; k++)
			{
				result += Coefs( Mesh[i].BubbleID[k] ) * basis(point,k+1);
			}
		}
		
		
		
	}
	
	
	return result;
}


//Writes a given function in terms of the basis on the provided mesh.

Eigen::VectorXd InterpolateFunction(std::function<double(double)> func, Legendre_Polynomial basis, std::vector<Element> Mesh, double lbnd  = -5, double rbnd = 5)
{
	int basisCount = (Mesh[0].BubbleID.size() + 2)*Mesh.size(); // # of bubble funcs plus vertex funcs.
	
	Eigen::VectorXd Interpolant(basisCount);
	
	std::vector<double> evalPoints = basis.coefs; // Probably just some number of evaluation points.
	
	std::sort(evalPoints.begin(), evalPoints.end() );
	std::cout << "Interpolant is: " << Interpolant.rows() << std::endl;
	double epsiPoint;
	
	for(int elem=0; elem < Mesh.size(); elem++)
	{
		
		for(int co = 0; co <evalPoints.size(); co++ )
		{
			epsiPoint = evalPoints[co];
			
			if(co==0) // Then we are looking at a vertex basis function
			{
				Interpolant( Mesh[elem].GLOBAL_VERTEX_ID[0] ) = func( Mesh[elem].refMap(epsiPoint)  );
			}
			if(co==evalPoints.size()-1) // Last point?
			{
				Interpolant( Mesh[elem].GLOBAL_VERTEX_ID[1] ) = func( Mesh[elem].refMap(epsiPoint) ); 
			}
			else if(co < Mesh[elem].BubbleID.size())
			{
				// All other points are the bubble functions in ascending order.
				Interpolant( Mesh[elem].BubbleID[co] ) = func( Mesh[elem].refMap(epsiPoint) );
			}
		}
			
	}
	
	return Interpolant;
	
}
