#include <vector>



#ifndef UTILITY_FE_PROJECT_HPP
#define UTILITY_FE_PROJECT_HPP

std::vector<double> GenerateChebyshevPoints(unsigned int poly_degree)
{
	std::vector<double> out;
	
	unsigned int points = poly_degree + 1;
	for (int j = 1; j<= points; j++)
	{
			out.push_back( cos( (3.14*(j-1) )/points ) );
	}
	return out;
}

struct Legendre_Polynomial {
	
	
	std::vector<double> coefs;
	int size;
	Legendre_Polynomial(){};
	
	
	void SetCoeffecients(std::vector<double> p_coefs) 
	{
		coefs = p_coefs;
		size = p_coefs.size();
	}
	double operator()(double x, int sub_script)
	{
		// subscript should be between 1 and p+1
		double result=1;
		for(int i=0; i < coefs.size(); i++) 
		{	
			if(i!=sub_script)
				result = result *  ( (x - coefs[i] )/ ( coefs[sub_script] - coefs[i] ) );
		}
		return result;
	}
};

Legendre_Polynomial GenerateBasis(int polynomialDegree)
{
	Legendre_Polynomial result; 
	result.SetCoeffecients( GenerateChebyshevPoints(polynomialDegree) );
	return result;
}


struct Element {
	bool DIR_FLAG[2]; // Does dirilect condition apply to a vertex here?
	double DIR_VALUE[2]; // Only initialized if cooresponding DIR_FLAG is true.
	int GLOBAL_VERTEX_ID[2]; // Unique identifier for each vertex.
	std::vector<int> BubbleID;
	
	double LeftPoint, RightPoint;
	double Jac;
	
	void Jacobian() { this->Jac = (RightPoint-LeftPoint)/2;}
	
	double refMap(double epsi) // Transforms point within the interval (LeftPoint, RightPoint) into (-1,1).
	{
		return ((LeftPoint + RightPoint)/2) + ((RightPoint-LeftPoint)/2)*epsi;
	}
	Element() { DIR_FLAG[0] = false; DIR_FLAG[1] = false; } // Init, dir flags by default.
};

#endif
