#include <iostream>
#include <vector>


#include "Boost/math/tr1.hpp"

#include "Eigen/Sparse"
#include "Eigen/Cholesky"
#include "Eigen/Dense"


/*

Vanilla RK4 method, to step a single variable forward in time. 


*/

struct FE_Result 
{
	Eigen::MatrixXd Stiffness, MassMatrix ;
	Eigen::VectorXd ForceV, Y_nought, Y_prime;
};





Eigen::VectorXd ClassicRungeKutta(FE_Result pinits, double stepSize=0.1) //Single step forward.
{	
	Eigen::VectorXd result;
	Eigen::VectorXd k_1, k_2, k_3, k_4;
	
	auto st = pinits.Stiffness;
	auto mm = pinits.MassMatrix;
	auto Fv = pinits.ForceV;
	auto Y0 = pinits.Y_nought;
	
	k_1 = pinits.Y_prime;
	
	Eigen::MatrixXd temp = Fv - st*(Y0 + (stepSize/2)*k_1);
	
	k_2 = Eigen::LDLT<Eigen::MatrixXd>( pinits.MassMatrix ).solve( temp );
	
	temp = Fv - st*(Y0 + (stepSize/2)*k_2);
	
	k_3 = k_2 = Eigen::LDLT<Eigen::MatrixXd>( pinits.MassMatrix ).solve(temp);
	
	temp = Fv - st*(Y0 + stepSize*k_3);
	
	k_4 = k_3 = k_2 = Eigen::LDLT<Eigen::MatrixXd>( pinits.MassMatrix ).solve( temp ); 
	
	result = Y0 + (1/6.0)*stepSize*(k_1 + 2*k_2 + 2*k_3 + k_4);
	
	return result;
}